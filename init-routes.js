const { fetchPosts, addPost, getPosts }=  require('./controllers/posts-controller');

const initRoutes = (app) => {
    app
        .route('/')
         .get(getPosts)
         .post(addPost)
};



module.exports = { initRoutes };
