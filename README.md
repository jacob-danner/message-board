# message-board

# purpose
The purpose of this site is to display posts that users post to a public message board. Express.js is used to run the server, handle requests, communicate with the controllers/database, and render the page. Fetch is used to send POST and GET requests from the frontend page. 
A user enters their name and their post content into the input boxes. Upon pressing the submit button, the post is sent to the database.
The most recent posts are displayed below the entry boxes.
This has not been deployed. It can be accessed by running the server and going to http://localhost:8080/

Future plans would include the ability to upvote and downvote currently displayed posts.

# how to use
To use this message board start a server on localhost with the command node index.js and click on the link that is returned.

# files
https://gitlab.com/jacob-danner/message-board

# group members
Jacob Danner, Elena Kernen, Anna Adams, and Alejandro Dominguez
