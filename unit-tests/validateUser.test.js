// Unit-Tests
const validateUser = require('./validateUser');

test("test #1 user is null it should return false, because user < 8 character, but does not contain a letter or a number", () => {
    expect(validateUser("")).toBe(false)
})

test("test #2 user is 3 characters long it should return false, because user < 8 character, contains at least a number but not a character", () => {
    expect(validateUser("aks")).toBe(false)
})

test("test #3 user is 4 characters long it should return false, because user < 8 character, contains at least one number but does not contain a letter ", () => {
    expect(validateUser("1251")).toBe(false)
})

test("test #4 user is null it should return true, because user < 8 character, contains a letter and a number thus a valid user", () => {
    expect(validateUser("jack12")).toBe(true)
})





