// function that validates users
function validateUser(user) {
    // maximum 8 characters long
    const validLength = user.length <= 8
    // has at least one character
    let hasLetter = false
    const alphabet = "abcdefghijklmnopqrstuvwxyz"
    for (const letter of alphabet) {
        if (user.toLowerCase().includes(letter)) {
            hasLetter = true
        }
    }
    // at least one number
    let hasNumber = false
    const numbers = "0123456789"
    for (const number of numbers) {
        if (user.includes(number)) {
            hasNumber = true
        }
    }

    const validUser = hasNumber && hasLetter && validLength
    return validUser
}

module.exports = validateUser
