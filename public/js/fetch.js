async function sendPostRequest() {
    // Selecting the input element and get its value 
   let inputContent = document.getElementById("content").value;
   let inputUser = document.getElementById("user").value;
   console.log(inputContent);
   console.log(inputUser);
   let data = { user: inputUser, content: inputContent};
   // Displaying the value
   
   console.log(inputContent);
   setTimeout(() => 10000);
   const url = new URL("http://localhost:8080")

   const response = await fetch(url, {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    headers: {
      'Content-Type': 'application/json'
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: JSON.stringify(data) // body data type must match "Content-Type" header
  });
  console.log(response.json()); // parses JSON response into native JavaScript objects
  return response.json;
}
