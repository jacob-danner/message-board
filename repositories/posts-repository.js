const { postQuery, addPost } = require('./posts-promises');

let fs = require('fs');
       
const get = async() => {
    try {
        const posts = await postQuery();
        return posts;
    } catch(err) {
        console.log(err)
    }
}

const create = async(user, content) => {
    await addPost(user, content);
}

module.exports = { get, create}