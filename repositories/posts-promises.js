let fs = require('fs');

function postQuery() {
    return new Promise((resolve, reject) => {
        const allPosts = fs.readFileSync('./db/posts-database.txt', 'utf-8');
        const formattedPosts = [];
        if(allPosts) {
            // NEED TO format posts in someway
            const seperatedPosts = allPosts.split('\n');

            seperatedPosts.forEach(post => {
                // construct object from each string
                let indPost = JSON.parse(post);
                formattedPosts.push(indPost)
            });
            resolve(formattedPosts);
        } else {
            reject('error');
        }
    });
};

function addPost(user, content) {
    return new Promise((resolve, reject) => {
        //if valid post - NEED TO add test here to validate post
        let valid = true;
        if (valid) {
            // make json object with user and content, add to db
            const newPostObj = { 
                "user": user,
                "content": content
            };
            const postString = "\n" + JSON.stringify(newPostObj);
            fs.appendFileSync('./db/posts-database.txt', postString);
            resolve('success');

        } else {
            reject('error');
        };
    });
};

module.exports = {postQuery, addPost};