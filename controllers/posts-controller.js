const { get, create } = require('../repositories/posts-repository');

const getPosts = (req, res) => {
    get()
        .then((posts) => {
            const  data = {name: posts,
            posts: posts};
            //res.status(200).json(posts)
            res.render('home', {data:data});         
        })
        .catch((err) => res.status(500).json(err));
};

const addPost = (req, res) => {
    const { user, content } = req.body;
    create(user, content).then(() => {
        res.status(201).json({ status: `success`, message: `post added.` })
    })
}

module.exports = { getPosts, addPost };