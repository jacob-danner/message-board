// Integration-Tests
const postsRepository = require(`../repositories/posts-repository`);

describe(`posts repository`, () => {

    it(`should create a new person`, async () => {
        const user = `Steve345`;
        const content = `newest post`;

        await postsRepository.create(user, content);
        const rows = await postsRepository.get();

        expect(rows.length).toBe(rows.length);
        expect(rows[rows.length-1]).toStrictEqual({
            user,
            content,
        });

    });

    it(`should get the seeded posts`, async () => {
        const rows = await postsRepository.get();

        expect(rows.length).toBe(rows.length);
        expect(rows[0]).toStrictEqual({
        user: `elena`,
        content: `dog`

        });
    });
});
