const express = require('express');
const ejs = require('ejs');
const cors = require("cors")

const { initRoutes } = require(`./init-routes`);

const app = express();
const port = 8080;

app.use(cors())
app.use(express.json());
app.set('view engine', 'ejs');
app.use(express.static("public"));
initRoutes(app);

app.listen(port)
console.log(`Server started at http://localhost:${port}`);